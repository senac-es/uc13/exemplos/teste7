package br.com.senac.test;

import br.com.senac.ex5.ClassifacaNumero;
import org.junit.Test;
import static org.junit.Assert.*;

public class ClassificaNumeroTest {

    public ClassificaNumeroTest() {
    }

    @Test
    public void numero4deveSerPar() {
        int numero = 4;
        boolean resultado = ClassifacaNumero.isPar(numero);
        assertTrue(resultado);
    }

    @Test
    public void numero5NaoDeveSerPar() {
        int numero = 5;
        boolean resultado = ClassifacaNumero.isPar(numero);
        assertFalse(resultado);
    }

    @Test
    public void numero10DeveSerPosivo() {
        int numero = 10;
        boolean resultado = ClassifacaNumero.isPositivo(numero);
        assertTrue(resultado);
    }
    
    public void numero10NegativoDeveSerPosivo(){
         int numero = -10;
        boolean resultado = ClassifacaNumero.isPositivo(numero);
        assertFalse(resultado);
    }

}
