package br.com.senac.ex5;

import java.util.Scanner;

public class ClassifacaNumero {

    public static void main(String[] args) {

        int numero;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Digite Numero:");
        numero = scanner.nextInt();

        if (isPar(numero)) {
            System.out.print("O Numero e Par");
        } else {
            System.out.print("O Numero e Impar");
        }

        if (isPositivo(numero)) {
            System.out.println(" e positivo");
        } else {
            System.out.println(" e negativo");
        }

    }

    public static boolean isPar(int numero) {
        return numero % 2 == 0;
    }

    public static boolean isPositivo(int numero) {
        return numero >= 0;
    }

}
